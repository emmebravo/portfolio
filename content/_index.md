---
title: 'mbcodes: welcome!'
description: 'bienvenidos.'
---

### Welcome!

I'm a full-stack SWE by way of theatre and film.
Creating is how I relate to the world.
Singing and reading is how I pass my time.
Thanks for stopping by.

{{< button href="/" target="_self" aria-label="Download Resume" >}} Download Resume{{< /button >}}

<!-- #### Technologies I've used -->
