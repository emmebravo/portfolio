---
title: 'Contact Me'
showDate: false
showReadingTime: false
showAuthor: false
---

{{< rawhtml >}}

<form name="contact" method="POST" netlify-honeypot="bot-field" data-netlify-recaptcha="true" data-netlify="true">
    <input type="hidden" name="form-name" value="contact" />
    <!-- Text input-->
    <div class="mb-3 pt-0 flex flex-col">
        <label for="name">Name (required)</label>
        <input id="name" class="w-1/3 border border-4 px-4 py-2" type="text" placeholder="Your name" name="name" required />
    </div>
    <div class="mb-3 pt-0 flex flex-col">
        <label for="email">Email (required)</label>
        <input id="email" class="w-1/3 border border-4 px-4 py-2" type="email" placeholder="Email" name="email" required />
    </div>
    <div class="mb-3 pt-0 flex flex-col">
        <label for="message">Message (required)</label>
        <textarea id="message" class="border border-4 w-1/3 px-4 py-2" placeholder="Your message" name="message" required></textarea>
    </div>
    <div data-netlify-recaptcha="true"></div>
    <div class="mb-3 pt-0">
       <button class="border border-2 px-6 py-2">Submit</button>
    </div>
    </form>
</form>

{{< /rawhtml >}}
