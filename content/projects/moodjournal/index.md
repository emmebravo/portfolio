---
title: 'Mood Journal'
description: 'Turn your Playbills into digital archives'
feature: '*moodjournal*'
---

App with a daily check-in/quick quiz about how you’re feeling mentally and physically. Each check-in is scored and assigned a color which will display a graph for a visual representation of it.

GitHub: [Mood Journal](https://github.com/HattieTavares/mood-journal)

**FE: HTML, EJS, TailwindCSS, ES6; BE: Node/Express, MongoDB, Passport.js**
