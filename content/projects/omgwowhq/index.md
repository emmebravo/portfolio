---
title: 'OMG WOW *'
externalUrl: 'https://omgwowhq.org//'
description: 'Wordpress website for nonprofit, OMG Women Outreaching Women'
feature: '*omgwowhq*'
date: 2024-04-30
_build:
  render: 'false'
---

As part of 48in48, a cross-functional team delivers a WordPress responsive, accessibility compliant website for a non-profit; OMG Women Outreaching Women's mission is to empower Black women through education, mentorship and community.
