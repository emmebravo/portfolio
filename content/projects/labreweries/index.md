---
title: 'LA Breweries'
description: 'Simple, simple app'
feature: '*breweries*'
icons: 'js-regular'
---

A simple React SPA with information on local LA breweries. A React.js frontend with an Express.js backend using Breweries API information. Utilizes GoogleMaps API to show brewery location on map. If I were to redo it, I'd probably use Next.js instead of plain React.

GitHub: [LA Breweries](https://github.com/emmebravo/hcd-brewery)

**FE: React + CRA, MUI, GoogleMaps API; BE: Node/Express, Breweries API**
