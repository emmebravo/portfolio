---
title: 'See Me Fund *'
externalUrl: 'https://www.seemefund.org/'
description: 'Wordpress website for nonprofit, See Me Fund'
feature: '*seemefund*'
date: 2023-04-30
_build:
  render: 'false'
---

As part of 48in48, a cross-functional team delivers a WordPress responsive, accessibility compliant website for a non-profit; See Me Fund is a social impact fund increasing access to capital for female entrepreneurs, promoting equity and opportunity in their community.
