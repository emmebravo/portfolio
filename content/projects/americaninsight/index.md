---
title: 'American Insight *'
externalUrl: 'https://americaninsight.org/'
description: 'Wordpress website for nonprofit, American Insight'
feature: '*americaninsight*'
date: 2023-10-30
_build:
  render: 'false'
---

As part of 48in48, a cross-functional team delivers a WordPress responsive, accessibility compliant website for a non-profit; American Insight mission is to promote the history and values of Free Speech, Human Rights, and the Rule of Law.
