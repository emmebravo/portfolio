---
title: 'Theatre Bills'
description: 'Turn your Playbills into digital archives'
feature: '*theatrebills*'
---

Digitize your Playbills: keep the memories not the clutter!

As someone who was active in the theatre community pre-panini, as an actor and audience, collecting Playbills is a thing. Eventually, they became too much, especially those for smaller productions. I wanted to keep the memory, but not the clutter, so I decided to digitize them.

<!-- Visit [Theatre Bills](https://theatrebills.onrender.com) (Render takes a bit to load) -->

GitHub: [Theatre Bills](https://github.com/emmebravo/theatre_bills)

**FE: React + Vite, GoogleMaps API, TailwindCSS; BE: Node/Express, MongoDB, Cloudinary, Multer**
