---
title: 'Projects'
description: 'Learn about some of my projects.'
cascade:
  showDate: false
  showReadingTime: false
---

Client projects are denoted by an asterisk (\*). The others are personal projects.
